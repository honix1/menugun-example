﻿using MenuGun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName ="SettingsExample", menuName ="Settings/Example")]
public class SettingsExample : PersistentScriptableObject {

    public enum Choose {
        ChooseOne,
        ChooseTwo,
        ChooseThree
    }

    [Header("Some Settings")]
    [MenuHeader("Some Settings")]
    public Choose someEnum = Choose.ChooseOne;

    public string someString = "Long-long-long name";

    [MenuRange(-5, 30)]
    public int someInt = 42;

    [MenuRange(0, 2)]
    public float someFloat = 3.14f;

    public bool someBoolean = true;

    [Header("Another Settings")]
    [MenuHeader("Some Settings")]
    public Vector2 someVector2 = new Vector2();

    public Vector3 someVector3 = new Vector3();

    private int panda = 0;
    [MenuHeader("Property")]
    [MenuRange(0, 100)]
    public int Panda {
        get { return panda; }
        set { Debug.Log("Panda is " + panda); panda = value; }
    }
}
