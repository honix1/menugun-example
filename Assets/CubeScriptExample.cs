﻿using MenuGun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeScriptExample : MonoBehaviour {

    private float x;

    [MenuRange(-10, 10)]
    public float X {
        get { return x; }
        set {
            x = value;
            transform.position = new Vector3(x, transform.position.y, transform.position.z);
        }
    }

    public void Roll() {
        StartCoroutine(RollCoroutine(100));
    }

    private IEnumerator RollCoroutine(float mag) {
        float t = 0;
        while(t < 360) {
            float delta = Time.deltaTime * mag;
            transform.Rotate(0, 0, delta);
            t += delta;

            yield return null;
        }
    }
}
