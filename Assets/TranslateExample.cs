﻿using UnityEngine;
using System.Collections;
using MenuGun;

public class TranslateExample : MonoBehaviour {

    public MenuGunHub menugun;

    public string prefix = "~";

    void Start() {
        StartCoroutine(LateStart());
    }

    private IEnumerator LateStart() {
        yield return new WaitForEndOfFrame();

        Translate();
    }

    public void Translate() {
        menugun.DoTranslation(x => prefix + x);
    }
}
