### menugun-example
This is example project for [menugun](https://gitlab.com/honix1/menugun). Auto-generated in-game inspector for Unity.

### how to clone git repo with submodules
```
git clone git@gitlab.com:honix1/menugun-example.git
cd menugun-example
git submodule init
git submodule update
```